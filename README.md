# Uinput Module Kernel.

Alpine linux doesn't enable `User level driver support` to build uinput module kernel by default. I just extract some files from kernel source code which required to build the kernel module (uinput). I use uinput module for some tools (e.g wlrctl and wtype.).

## Dependencies ?

In order to build the module, I think you need to install `linux-lts-dev` package.

	```
	$ sudo apk add linux-lts-dev
	```

## How to build ?

- Clone this repository (obviously) !
- Run `make` to build the module !

	```
	$ make
	```

- Copy the module (uinput.ko) to `/lib/modules/kernel_version/kernel/` ! You can check kernel version by using `uname -r` command.

	```
	sudo cp -r uinput.ko /lib/modules/kernel_version/kernel/
	```

- Run `sudo depmod -a` and `sudo modprobe uinput`.
